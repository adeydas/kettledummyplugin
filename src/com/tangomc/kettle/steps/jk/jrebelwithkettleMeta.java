package com.tangomc.kettle.steps.jk;

import java.util.List;
import java.util.Map;

import org.eclipse.swt.widgets.Shell;
import org.pentaho.di.core.CheckResult;
import org.pentaho.di.core.CheckResultInterface;
import org.pentaho.di.core.Counter;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.core.variables.VariableSpace;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepDialogInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.w3c.dom.Node;

public class jrebelwithkettleMeta extends BaseStepMeta implements
		StepMetaInterface {
	// Define the variable to store values from the UI. Define getters and
	// setters.

	private String param;

	public void setparam(String var) {
		this.param = var;
	}

	public String getparam() {
		return this.param;
	}

	@Override
	public void check(List<CheckResultInterface> remarks, TransMeta transmeta,
			StepMeta stepMeta, RowMetaInterface prev, String[] input,
			String[] output, RowMetaInterface info) {
		CheckResult cr;
		if (prev == null || prev.size() == 0) {
			cr = new CheckResult(CheckResult.TYPE_RESULT_WARNING,
					"Not receiving any fields from previous steps!", stepMeta);
			remarks.add(cr);
		} else {
			cr = new CheckResult(CheckResult.TYPE_RESULT_OK,
					"Step is connected to previous one, receiving "
							+ prev.size() + " fields", stepMeta);
			remarks.add(cr);
		}
		// See if we have input streams leading to this step!
		if (input.length > 0) {
			cr = new CheckResult(CheckResult.TYPE_RESULT_OK,
					"Step is receiving info from other steps.", stepMeta);
			remarks.add(cr);
		} else {
			cr = new CheckResult(CheckResult.TYPE_RESULT_ERROR,
					"No input received from other steps!", stepMeta);
			remarks.add(cr);
		}
	}

	@Override
	public String getXML() {
		StringBuffer retval = new StringBuffer();
		// Return the Xml to store the UI properties and data. Commented example
		// below.
		/*
		 * retval.append("    ").append(XMLHandler.addTagValue(getXmlCode("ID"),
		 * Integer.toString(CorrespondingUIElement)));
		 */
		return retval.toString();
	}

	@Override
	public Object clone() {
		Object retval = super.clone();
		return retval;
	}

	@Override
	public void getFields(RowMetaInterface r, String origin,
			RowMetaInterface[] info, StepMeta nextStep, VariableSpace space) {
		ValueMetaInterface v;
		// Set the output fields if this is a transformation step. Commented
		// example below
		/*
		 * v = ValueMetaFactory.createValueMeta("SomeVal",
		 * ValueMeta.TYPE_NUMBER); v.setLength(15); v.setPrecision(6);
		 * v.setOrigin(origin); r.addValueMeta(v);
		 */
	}

	@Override
	public StepInterface getStep(StepMeta stepMeta,
			StepDataInterface stepDataInterface, int cnr, TransMeta transMeta,
			Trans disp) {
		return new jrebelwithkettle(stepMeta, stepDataInterface, cnr,
				transMeta, disp);
	}

	@Override
	public StepDataInterface getStepData() {
		return new jrebelwithkettleData();
	}

	@Override
	public void loadXML(Node stepnode, List<DatabaseMeta> databases,
			Map<String, Counter> counters) {
		// Get data from Xml and populate GUI elements
		/*
		 * GUIElementVar =
		 * Integer.parseInt(XMLHandler.getTagValue(stepnode,getXmlCode
		 * ("XmlAttr")));
		 */
	}

	public void readRep(Repository arg0, long arg1, List<DatabaseMeta> arg2,
			Map<String, Counter> arg3) throws KettleException {
		// Code to read step state from a repository
	}

	public void saveRep(Repository arg0, long arg1, long arg2)
			throws KettleException {

		// Code to save step state to a repository
	}

	@Override
	public void setDefault() {
		// Default values for GUI elements
	}

	public StepDialogInterface getDialog(Shell shell, StepMetaInterface meta,
			TransMeta transMeta, String name) {
		return new jrebelwithkettleDialog(shell, meta, transMeta, name);
	}
}
