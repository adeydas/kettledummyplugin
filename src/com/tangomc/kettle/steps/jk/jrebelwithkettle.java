package com.tangomc.kettle.steps.jk;

import org.pentaho.di.core.Const;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStep;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;

public class jrebelwithkettle extends BaseStep implements StepInterface {
	private jrebelwithkettleData data;
	private jrebelwithkettleMeta meta;

	public jrebelwithkettle(StepMeta stepMeta,
			StepDataInterface stepDataInterface, int copyNr,
			TransMeta transMeta, Trans trans) {
		super(stepMeta, stepDataInterface, copyNr, transMeta, trans);
	}

	public synchronized boolean processRow(StepMetaInterface smi,
			StepDataInterface sdi) throws KettleException {
		meta = (jrebelwithkettleMeta) smi;
		data = (jrebelwithkettleData) sdi;

		Object[] r = getRow();
		if (r == null) {
			// No more rows left
			setOutputDone();
			return false;
		}

		if (first) {
			first = false;
			// Good place to set the meta fields
		}
		return true;
	}

	public boolean init(StepMetaInterface smi, StepDataInterface sdi) {
		meta = (jrebelwithkettleMeta) smi;
		data = (jrebelwithkettleData) sdi;
		// All initialization code goes here
		return super.init(smi, sdi);
	}

	public void dispose(StepMetaInterface smi, StepDataInterface sdi) {
		meta = (jrebelwithkettleMeta) smi;
		data = (jrebelwithkettleData) sdi;
		super.dispose(smi, sdi);
	}

	public void run() {
		try {
			while (processRow(meta, data) && !isStopped())
				;
		} catch (Exception e) {
			logError(Const.getStackTracker(e));
			setErrors(1);
			stopAll();
		} finally {
			dispose(meta, data);
			markStop();
		}
	}
}
