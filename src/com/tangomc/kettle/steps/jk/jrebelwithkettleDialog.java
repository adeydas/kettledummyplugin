package com.tangomc.kettle.steps.jk;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.pentaho.di.core.Const;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepDialogInterface;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.ui.trans.step.BaseStepDialog;

public class jrebelwithkettleDialog extends BaseStepDialog implements
		StepDialogInterface {
	private jrebelwithkettleMeta input;
	private Label wlparam;
	private Text wparam;
	private FormData fdlparam, fdparam;

	public jrebelwithkettleDialog(Shell parent, Object baseStepMeta,
			TransMeta transMeta, String stepname) {
		super(parent, (StepMetaInterface) baseStepMeta, transMeta, stepname);
		input = (jrebelwithkettleMeta) baseStepMeta;
	}

	@Override
	public String open() {
		Shell parent = getParent();
		Display display = parent.getDisplay();
		shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.RESIZE | SWT.MIN
				| SWT.MAX);
		props.setLook(shell);
		setShellImage(shell, input);

		ModifyListener lsMod = new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				input.setChanged();
			}
		};
		changed = input.hasChanged();
		FormLayout formLayout = new FormLayout();
		formLayout.marginWidth = Const.FORM_MARGIN;
		formLayout.marginHeight = Const.FORM_MARGIN;
		shell.setLayout(formLayout);
		shell.setText(Messages.getString("JRebel with Kettle"));
		int middle = props.getMiddlePct();
		int margin = Const.MARGIN;

		// Stepname line
		wlStepname = new Label(shell, SWT.LEFT);
		wlStepname.setText(Messages.getString("JRebel with Kettle")); //$NON-NLS-1$
		props.setLook(wlStepname);
		fdlStepname = new FormData();
		fdlStepname.left = new FormAttachment(0, 0);
		fdlStepname.right = new FormAttachment(middle, -margin);
		fdlStepname.top = new FormAttachment(0, margin);
		wlStepname.setLayoutData(fdlStepname);
		wStepname = new Text(shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		wStepname.setText(stepname);
		props.setLook(wStepname);
		wStepname.addModifyListener(lsMod);
		fdStepname = new FormData();
		fdStepname.left = new FormAttachment(middle, 0);
		fdStepname.top = new FormAttachment(0, margin);
		fdStepname.right = new FormAttachment(100, 0);
		wStepname.setLayoutData(fdStepname);

		wlparam = new Label(shell, SWT.LEFT);
		wlparam.setText(Messages.getString("jrebelwithkettle.param.Title"));
		props.setLook(wlparam);
		fdlparam = new FormData();
		fdlparam.left = new FormAttachment(0, 0);
		fdlparam.right = new FormAttachment(middle, -margin);
		fdlparam.top = new FormAttachment(wStepname, margin);
		wlparam.setLayoutData(fdlparam);
		wparam = new Text(shell, SWT.LEFT);
		props.setLook(wparam);
		wparam.addModifyListener(lsMod);
		fdparam = new FormData();
		fdparam.left = new FormAttachment(middle, 0);
		fdparam.right = new FormAttachment(100, 0);
		fdparam.top = new FormAttachment(wStepname, margin);
		wparam.setLayoutData(fdparam);
		// Buttons
		wOK = new Button(shell, SWT.PUSH);
		wOK.setText(Messages.getString("System.Button.OK"));
		wCancel = new Button(shell, SWT.PUSH);
		wCancel.setText(Messages.getString("System.Button.Cancel"));
		BaseStepDialog.positionBottomButtons(shell,
				new Button[] { wOK, wCancel }, margin, wparam);
		// Listeners
		lsCancel = new Listener() {
			public void handleEvent(Event e) {
				cancel();
			}
		};
		lsOK = new Listener() {
			public void handleEvent(Event e) {
				ok();
			}
		};
		wCancel.addListener(SWT.Selection, lsCancel);
		wOK.addListener(SWT.Selection, lsOK);
		lsDef = new SelectionAdapter() {
			public void widgetDefaultSelected(SelectionEvent e) {
				ok();
			}
		};
		wStepname.addSelectionListener(lsDef);
		shell.addShellListener(new ShellAdapter() {
			public void shellClosed(ShellEvent e) {
				cancel();
			}
		});
		setSize();
		getData();
		input.setChanged(changed);
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		return stepname;
	}

	// Read data from input (TextFileInputInfo)
	public void getData() {
		wStepname.selectAll();
		// Set data to UI elements here
	}

	private void cancel() {
		stepname = null;
		input.setChanged(changed);
		dispose();
	}

	private boolean isEmpty(String s) {
		return s.isEmpty();
	}

	private void ok() {
		stepname = wStepname.getText(); // return value
		if (Const.isEmpty(stepname))
			return;
		// Populate UI variables in meta here
	}
}
